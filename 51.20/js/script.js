"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến toàn cục để lưu trữ product id đang đc update or delete. Mặc định = 0;
var gProductId = 0;
// Mảng chứa các đối tượng Quốc gia và Nhóm sản phẩm để đổ data vào các thẻ Select trang Admin
var gCountriesObject = [];
var gProductCategoriesObject = [];
// Mảng chứa các đối tượng Quốc gia và Nhóm sản phẩm để đổ data vào các thẻ Select trong Create Modal
var gCountriesObjectCreateModal = [];
var gProductCategoriesObjectCreateModal = [];
// Mảng chứa các đối tượng Quốc gia và Nhóm sản phẩm để đổ data vào các thẻ Select trong Update Modal
var gCountriesObjectUpdateModal = [];
var gProductCategoriesObjectUpdateModal = [];
// Khai báo mảng chứa các Product để phục vụ cho việc 'Lọc Sản Phẩm'
var gListOfProducts = [];
// Khai báo biến toàn cục lưu giá trị thuộc tính src để tạo mới hoặc cập nhật
var gImgUrl = "";

const gBASE_URL = "https://63b401c1ea89e3e3db539072.mockapi.io/api/v1/";
const gCONTENT_TYPE = "application/json;charset=UTF-8";

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gPRODUCT_COLS = ["stt", "productCode", "productName", "categoryId", "countryId", "imagUrl", "price", "discountPrice", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gPRODUCT_STT_COL = 0;
const gPRODUCT_PRODUCT_CODE_COL = 1;
const gPRODUCT_PRODUCT_NAME_COL = 2;
const gPRODUCT_PRODUCT_CATEGORY_COL = 3;
const gPRODUCT_COUNTRY_COL = 4;
const gPRODUCT_AVATAR_COL = 5;
const gPRODUCT_PRICE_COL = 6;
const gPRODUCT_DISCOUNT_PRICE_COL = 7;
const gPRODUCT_ACTION_COL = 8;

// Cấu hình Firebase SDK với thông tin của dự án Firebase
const gFirebaseConfig = {
  apiKey: "AIzaSyDMcJAAry1dA6vIX2hYmzLrsSKAnXWNbs4",
  authDomain: "photouploadstorage-716e8.firebaseapp.com",
  projectId: "photouploadstorage-716e8",
  storageBucket: "photouploadstorage-716e8.appspot.com",
  messagingSenderId: "297619420631",
  appId: "1:297619420631:web:d1120f69a29ec54bc58792",
  measurementId: "G-CVHX4ED5CQ"
};

// Khởi tạo Firebase
firebase.initializeApp(gFirebaseConfig);

// Biến toàn cục để hiển thị STT
var gSTT = 1;
// Khai báo DataTable & mapping collumns
var gProductTable = $("#product-table").DataTable({
  // Phân trang DataTable
  paging: true,
  // Bỏ thay đổi số dòng trên mỗi trang của bảng
  lengthChange: false,
  // Bỏ cho phép tìm kiếm trên bảng
  searching: false,
  // Không cho phép sắp xếp trên bảng
  ordering: false,
  columns: [
    { data: gPRODUCT_COLS[gPRODUCT_STT_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_PRODUCT_CODE_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_PRODUCT_NAME_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_PRODUCT_CATEGORY_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COUNTRY_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_AVATAR_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_PRICE_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_DISCOUNT_PRICE_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_ACTION_COL] },
  ],
  columnDefs: [
    { // định nghĩa lại cột STT
      targets: gPRODUCT_STT_COL,
      render: function () {
        return gSTT++;
      }
    },
    {
      targets: gPRODUCT_PRODUCT_CATEGORY_COL,
      render: renderProductCagoriesByCategoryId
    },
    {
      targets: gPRODUCT_COUNTRY_COL,
      render: renderCountryNamesByCountryId
    },
    {
      targets: gPRODUCT_AVATAR_COL,
      render: function(data) {
        // Trả về thẻ <img> để hiển thị hình ảnh
        return '<img src="' + data + '" alt="Hình ảnh" width="100" height="100">';
      }
    },
    { // định nghĩa lại cột action
      targets: gPRODUCT_ACTION_COL,
      defaultContent: `
      <img class="edit-product" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
      <img class="delete-product" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">`
    }
  ]
});

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  // thực hiện tải trang
  onPageLoading();

  // sự kiện nhấn vào button "Thêm mới" ở trang Admin Sản phẩm
  $("#btn-add-product").on("click", function() {
    onBtnAddNewClick();
  });

  // sự kiện ấn vào icon Bút chì để chọn 1 sản phẩm
  $("#product-table").on("click", ".edit-product", function() {
    onIconEditClick(this);
  });

  // sự kiện ấn vào icon Recycle để chọn 1 sản phẩm
  $("#product-table").on("click", ".delete-product", function() {
    onIconDeleteClick(this);
  });

  // 2. CREATE - sự kiện ấn vào button "Thêm mới" để tạo mới sản phẩm
  $("#btn-create-product").on("click", function() {
    onBtnCreateNewProductModalClick();
  });

  // 3. UPDATE - sự kiện ấn vào button "Cập nhật" để cập nhật thông tin mới của Product
  $("#btn-update-product").on("click", function() {
    onBtnUpdateProductModalClick();
  });

  // 4. DELETE - sự kiện ấn vào button "Xác nhận" để xóa sản phẩm theo Product Id
  $("#btn-confirm-delete-product").on("click", function() {
    onBtnDeleteExistingProductModalClick();
  });

  // sự kiện ấn vào nút button "Lọc sản phẩm"
  $("#btn-filter-product").on("click", function() {
    onBtnFilterProducts();
  });

  // sự kiện nhấn nút 'Tải ảnh lên' ở Create Modal
  $("#file-create-upload").on("change", function(event) {
    onFileUploadPhotoCreatingChange(event);
  });

  // Sự kiện nhấn nút 'Tải ảnh lên' ở Update Modal
  $("#file-update-upload").on("change", function(event) {
    onFileUploadPhotoUpdatingChange(event);
  });

});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm sự kiện thực thi khi trang được load
function onPageLoading() {
  // Gọi trước 2 function tải data vào thẻ Select trước khi tải danh sách Product
  Promise.all([getAllCountries(), getAllProductCagories()])
  .then(function () {
    // Sau khi cả hai hàm trên đã hoàn thành, gọi hàm getAllProducts()
    getAllProducts();
  })
  .catch(function (error) {
    // Xử lý lỗi nếu cần thiết
    console.error("Error:", error);
  });
}
// hàm sự kiện thêm mới sản phẩm trong Product Create Modal
function onBtnCreateNewProductModalClick() {
  // Khởi tạo đối tượng Product
  var vProduct = initializeProductObject();
  // 1. Đọc dữ liệu
  getCreateNewInputInfor(vProduct);
  // 2. Kiểm tra dữ liệu
  var vValid = validateInputProduct(vProduct);
  if(vValid) {
    // 3. Gọi API
    callAPICreateProductDetail(vProduct);
    // 4. Xử lý hiển thị thông tin sau khi cập nhật Product thành công
    // thực hiện ở hàm displayDataWhenCreatedTheProductSuccessful sau khi cập nhật sản phẩm với thông tin mới thành công
  }
}
// hàm sự kiện cập nhật thông tin mới cho Product khi nhấn vào button Cập Nhật trong Product Update Modal
function onBtnUpdateProductModalClick() {
  // Khởi tạo đối tượng Product
  var vProduct = initializeProductObject();
  // 1. Đọc dữ liệu
  getUpdateCurrentProductInputInfor(vProduct);
  // 2. Kiểm tra dữ liệu
  var vValid = validateInputProduct(vProduct);
  if(vValid) {
    // 3. Gọi API
    callAPIUpdateProductDetail(vProduct);
  }
  // 4. Xử lý hiển thị thông tin sau khi cập nhật Product thành công
  // thực hiện ở hàm displayDataWhenUpdatedTheProductSuccessful sau khi cập nhật sản phẩm với thông tin mới thành công
}
// hàm sự kiện xóa một sản phẩm đang tồn tại khi ấn vào button Xác nhận trong Product Delete Modal
function onBtnDeleteExistingProductModalClick() {
  // 1. Đọc dữ liệu (bỏ qua bước này)
  // 2. Kiểm tra dữ liệu (bỏ qua bước này)
  // 3. Gọi API
  callAPIDeleteExistingProductById();
  // 4. Xử lý hiển thị thông tin sau khi cập nhật Product thành công
  // thực hiện ở hàm ... sau khi cập nhật sản phẩm với thông tin mới thành công
}
// hàm sự kiện nhấn vào button "Thêm Mới" trang Admin
function onBtnAddNewClick() {
  // Đổ data vào các thẻ Select trong Create Modal trước khi mở Modal lên
  pumpDataToProductCategorySelectInCreateModal();
  pumpDataToCountrySelectInCreateModal();
  // hiển thị Product Update Modal lên
  $("#create-product-modal").modal("show");
}
// hàm sự kiện chọn một sản phẩm và xem chi tiết thông tin
function onIconEditClick(paramClickedIcon) {
  // lưu thông tin productId đang được edit vào biến toàn cục
  gProductId = getProductIdFromClickedIcon(paramClickedIcon);
  // load thông tin vào các trường dữ liệu trong Update Modal
  getProductDetailByProductId(gProductId);
  // hiển thị Product Update Modal lên
  $("#update-product-modal").modal("show");
}
//hàm sự kiện chọn một sản phẩm và lựa chọn xóa hay không
function onIconDeleteClick(paramClickedIcon) {
  // lưu thông tin productId đang được delete vào biến toàn cục
  gProductId = getProductIdFromClickedIcon(paramClickedIcon);
  // lấy đối tượng đã tạo chứa thông tin productName và imagUrl để hiển thị lên Modal
  var vTempObj = getSomeNecessaryInforFromClickedIcon(paramClickedIcon);
  displayTheContentOfElementsInDeleteModal(vTempObj);
  // hiển thị Product Delete Modal lên
  $("#delete-confirm-modal").modal("show");
}
// hàm sự kiện lọc danh sách Products theo Country Name và Product Category
function onBtnFilterProducts() {
  // khởi tạo một mảng chứa các sản phẩm đã tìm được
  var vProductArr = [];
  // tạo biến chứa giá trị của Country Name và Product Category
  var vCountryId = "";
  var vCategoryId = "";
  // lấy value của từng thẻ Select 'Quốc gia' và 'Nhóm sản phẩm
  var vCountrySelectValue = $("#country-select").val();
  var vProductCategorySelectValue = $("#product-category-select").val();
  // lấy ra id dựa vào value của thẻ option
  gCountriesObject.forEach(function(countryItem) {
    if(countryItem.countryCode == vCountrySelectValue) {
      // gán Country Name có trong mảng đối tượng cho biến vCountryName
      vCountryId = countryItem.id;
    }
  });
  gProductCategoriesObject.forEach(function(productCategoryItem) {
    if(productCategoryItem.categoryCode == vProductCategorySelectValue) {
      // gán Product Category có trong mảng đối tượng cho biến vProductCategory
      vCategoryId = productCategoryItem.id;
    }
  });

  // kiểm tra điều kiện để hiển thị các Product thỏa
  for(let bIndex = 0; bIndex < gListOfProducts.length; bIndex ++) {
    let bCurrentProduct = gListOfProducts[bIndex];
    // Khai báo và khởi tạo một mảng chứa các điều kiện
    let bConditions = [
      // Trường hợp 1: chỉ khớp countryId
      bCurrentProduct.countryId == vCountryId,
      // Trường hợp 2: chỉ khớp categoryId
      bCurrentProduct.categoryId == vCategoryId,
      // Trường hợp 3: khớp cả countryId và categoryId
      bCurrentProduct.countryId == vCountryId && bCurrentProduct.categoryId == vCategoryId,
      // Trường hợp 4: khớp cả 1 trong 2 hoặc khớp cả hai
      (bCurrentProduct.countryId == vCountryId || bCurrentProduct.categoryId == vCategoryId
      || bCurrentProduct.countryId == vCountryId && bCurrentProduct.categoryId == vCategoryId)
    ];
    // Kiểm tra nếu một trong các điều kiện ở trên thỏa thì thêm vào mảng chứa đối tượng Product
    if(bConditions.some(cond => cond)) {  // phương thức some: ít nhất một phần tử trong mảng thỏa điều kiện
      // thêm đối tượng Product vừa tìm được vào mảng vừa khởi tạo trong hàm
      vProductArr.push(bCurrentProduct);
    }
  }
  console.log(vProductArr);
  // gọi hàm fillData... để tải lên các đối tượng (User row) thỏa điều kiện lên table
  if(vProductArr.length > 0) {
    // Xóa tất cả dữ liệu trong bảng DataTable trước khi thêm các dòng Product vừa tìm được
    gProductTable.clear().draw();
    // Thêm mảng vProductArr vào lại bảng
    gProductTable.rows.add(vProductArr).draw();
  }
  else {
    alert("Không tìm thấy Sản Phẩm phù hợp!");
    // hiển thị nội dung option mặc định cho các thẻ Select ở Admin page
    $("#country-select").val("all");
    $("#product-category-select").val("all");
    // tải lại trang khi không tìm thấy Sản phẩm nào
    getAllProducts();
  }
}
// hàm bao bọc sự kiện change Photo với async/await
async function onFileUploadPhotoCreatingChange(paramEvent) {
  try {
    var vSelectedFile = paramEvent.target.files[0];

    // kiểm tra phần mở rộng của ảnh tải lên có thỏa điều kiện hay không
    var vFileType = vSelectedFile.type;
    var vFileName = vSelectedFile.name;
    // Kiểm tra phần mở rộng của tên tệp
    var vFileExtension = vFileName.slice((vFileName.lastIndexOf(".") - 1 >>> 0) + 2);
    console.log("vFileExtension: " + vFileExtension);
    // Kiểm tra xem định dạng tệp có phải là ảnh không
    if (!vFileType.startsWith('image/') || !['jpg', 'jpeg', 'png', 'gif', 'svg'].includes(vFileExtension)) {
      throw new Error('Định dạng tệp không hợp lệ. Chỉ chấp nhận các tệp ảnh (.jpg, .png, .gif, .jpeg, .svg)');
    }
    
    var vImgUrl = URL.createObjectURL(vSelectedFile);
    $("#create-upload-image").attr("src", vImgUrl);
    // Gọi hàm trả về Promise trước đó và sử dụng await để đợi kết quả
    gImgUrl = await uploadImageToStorageWithPromise(vSelectedFile);
    // Tại đây, bạn có thể sử dụng đường dẫn trực tiếp của hình ảnh (imgUrl) sau khi tải lên thành công
  } catch (error) {
    console.error('Lỗi khi tải ảnh lên:', error);
    // lấy giá trị của src để gán cho biến toàn cục để hiển thị ở form Product Detail
    gImgUrl = $("#create-upload-image").attr("src");
    // Xử lý lỗi ở đây, ví dụ hiển thị thông báo lỗi cho người dùng.
  }
}
// hàm bao bọc sự kiện change Photo với async/await
async function onFileUploadPhotoUpdatingChange(paramEvent) {
  try {
    var vSelectedFile = paramEvent.target.files[0];

    // kiểm tra phần mở rộng của ảnh tải lên có thỏa điều kiện hay không
    var vFileType = vSelectedFile.type;
    var vFileName = vSelectedFile.name;
    // Kiểm tra phần mở rộng của tên tệp
    var vFileExtension = vFileName.slice((vFileName.lastIndexOf(".") - 1 >>> 0) + 2);
    console.log("vFileExtension: " + vFileExtension);
    // Kiểm tra xem định dạng tệp có phải là ảnh không
    if (!vFileType.startsWith('image/') || !['jpg', 'jpeg', 'png', 'gif', 'svg'].includes(vFileExtension)) {
      throw new Error('Định dạng tệp không hợp lệ. Chỉ chấp nhận các tệp ảnh (.jpg, .png, .gif, .jpeg, .svg)');
    }

    var vImgUrl = URL.createObjectURL(vSelectedFile);
    $("#update-upload-image").attr("src", vImgUrl);
    // Gọi hàm trả về Promise trước đó và sử dụng await để đợi kết quả
    gImgUrl = await uploadImageToStorageWithPromise(vSelectedFile);
    // Tại đây, bạn có thể sử dụng đường dẫn trực tiếp của hình ảnh (imgUrl) sau khi tải lên thành công
  } catch (error) {
    console.error('Lỗi khi tải ảnh lên:', error);
    // gán lại ảnh cũ khi ảnh mới upload lên không hợp lệ
    gImgUrl = $("#update-upload-image").attr("src");
    // Xử lý lỗi ở đây, ví dụ hiển thị thông báo lỗi cho người dùng.
  }
}
// hàm tải ảnh lên Firebase và lấy đường dẫn trực tiếp sau khi tải ảnh thành công
function uploadImageToStorageWithPromise(paramFile) {
  return new Promise((resolve, reject) => {
    // Tạo tham chiếu đến thư mục trong Firebase Storage mà bạn muốn tải lên hình ảnh
    const vStorageRef = firebase.storage().ref().child('the-synthetic-product/photo' + paramFile.name);
    // Tải lên hình ảnh lên Firebase Storage
    vStorageRef.put(paramFile)
      .then((snapshot) => {
        console.log('Tải lên hình ảnh thành công!');
        // Lấy đường dẫn tới hình ảnh đã tải lên
        return vStorageRef.getDownloadURL();
      })
      .then((url) => {
        console.log('Đường dẫn trực tiếp của hình ảnh:', url);
        // gán đường dẫn URL của image vào biến toàn cục để lưu giá trị
        gImgUrl = url;
        resolve(url); // Trả về đường dẫn trực tiếp của hình ảnh
      })
      .catch((error) => {
        console.error('Lỗi khi tải lên hình ảnh:', error);
        reject(error); // Trả về lỗi nếu có lỗi xảy ra
      });
  });
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */

/* Phần xử lý cho Product DataTable */

// hàm khởi tạo đối tượng Product
function initializeProductObject() {
  // khai báo đối tượng Product
  var vProduct = {
    productCode: "",
    productName: "",
    price: 0.0,
    discountPrice: 0.0,
    categoryId: 0,
    countryId: 0,
    imagUrl: "",
    description: "",
  };
  return vProduct;
}
// hàm gọi api để lấy all danh sách sản phẩm
function getAllProducts() {
  $.ajax({
    url: gBASE_URL + "products/",
    type: "GET",
    success: function (responseProducts) {
      loadDataToProductTable(responseProducts);
      // gán nội dung trả về cho mảng các đối tượng sản phẩm
      gListOfProducts = responseProducts;
    },
    error: function (ajaxContext) {
      console.log(ajaxContext.status);
    }
  });
}
/** load product array to DataTable
 * in: product array
 * out: product table has data
 */
function loadDataToProductTable(paramProductArr) {
  gSTT = 1;
  gProductTable.clear();
  gProductTable.rows.add(paramProductArr);
  gProductTable.draw();
}
// hàm lấy danh sách các Quốc Gia từ API để đổ vào thẻ Select 'Chọn quốc gia'
function getAllCountries() {
  return new Promise(function (resolve, reject) {
    // gọi server (api)
    $.ajax({
      url: gBASE_URL + "/countries/",
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        displayDataToCountrySelect(data);
        resolve();
      },
      error: function (ajaxContext) {
        reject(ajaxContext.status);
      }
    });
  });
}
// hàm lấy danh sách các nhóm sản phẩm từ API để đổ vào thẻ Select 'Nhóm sản phẩm'
function getAllProductCagories() {
  return new Promise(function (resolve, reject) {
    // gọi server (api)
    $.ajax({
      url: gBASE_URL + "/categories/",
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        displayDataToProductCategorySelect(data);
        resolve();
      },
      error: function (ajaxContext) {
        reject(ajaxContext.status);
      }
    });
  });
}
// hàm đổ tất cả dữ liệu Group Products vào thẻ Select 'Nhóm sản phẩm'
function displayDataToProductCategorySelect(paramResponseText) {
  // nếu mảng ban đầu chưa có dữ liệu mới tiến hành đổ data vào
  if(gProductCategoriesObject.length === 0) {
    // gán nội dung trả về cho mảng các đối tượng Country đã khai báo
    gProductCategoriesObject = paramResponseText;
    // truy xuất đến phần tử thẻ Select 'Chọn quốc gia'
    var vProductCategorySelect = $("#product-category-select");
    // dùng vòng lặp for để duyệt qua từng phần tử trong mảng các đối tượng và đẩy data vào thẻ select
    for(let bI = 0; bI < gProductCategoriesObject.length; bI ++) {
      $("<option>", {
        value: gProductCategoriesObject[bI].categoryCode,
        text: gProductCategoriesObject[bI].categoryName
      }).appendTo(vProductCategorySelect);
    }
  }
}
// hàm đổ tất cả dữ liệu Country vào thẻ Select 'Chọn quốc gia'
function displayDataToCountrySelect(paramResponseText) {
  // nếu mảng ban đầu chưa có dữ liệu mới tiến hành đổ data vào
  if(gCountriesObject.length === 0) {
    // gán nội dung trả về cho mảng các đối tượng Country đã khai báo
    gCountriesObject = paramResponseText;
    // truy xuất đến phần tử thẻ Select 'Chọn quốc gia'
    var vCountrySelect = $("#country-select");
    // dùng vòng lặp for để duyệt qua từng phần tử trong mảng các đối tượng và đẩy data vào thẻ select
    for(let bI = 0; bI < gCountriesObject.length; bI ++) {
      $("<option>", {
        value: gCountriesObject[bI].countryCode,
        text: gCountriesObject[bI].countryName
      }).appendTo(vCountrySelect);
    }
  }
}
// hàm render và hiển thị tên Nhóm sản phẩm theo Category Id
function renderProductCagoriesByCategoryId(paramCategoryId) {
  var vProductCategory = "";
  var vFound = false;
  var vIndex = 0;
  while (!vFound && vIndex < gProductCategoriesObject.length) {
    if (gProductCategoriesObject[vIndex].id == paramCategoryId) {
      vFound = true;
      vProductCategory = gProductCategoriesObject[vIndex].categoryName;
    } 
    vIndex ++;
  }
  return vProductCategory;
}
// hàm render và hiển thị tên Quốc gia theo Country Id
function renderCountryNamesByCountryId(paramCountryId) {
  var vCountryName = "";
  var vFound = false;
  var vIndex = 0;
  while (!vFound && vIndex < gCountriesObject.length) {
    if (gCountriesObject[vIndex].id == paramCountryId) {
      vFound = true;
      vCountryName = gCountriesObject[vIndex].countryName;
    }
    vIndex ++;
  }
  return vCountryName;
}
/* Phần xử lý cho Modal Update Product */
// hàm dựa vào button detail (edit or delete) xác định đc id product
function getProductIdFromClickedIcon(paramClickedIcon) {
  var vTableRow = $(paramClickedIcon).parents("tr");
  var vProductRowData = gProductTable.row(vTableRow).data();
  return vProductRowData.id;
}
// hàm dựa vào button detail (edit or delete) xác định được một số thông tin cần thiết
function getSomeNecessaryInforFromClickedIcon(paramClickedIcon) {
  var vTemporaryObj = {
    imagUrl: "",
    productName: "",
  }
  var vTableRow = $(paramClickedIcon).parents("tr");
  var vProductRowData = gProductTable.row(vTableRow).data();
  // gán nội dung cho đối tượng đã tạo
  vTemporaryObj.imagUrl = vProductRowData.imagUrl;
  vTemporaryObj.productName = vProductRowData.productName;
  return vTemporaryObj;
}
// hàm lấy thông tin sản phẩm theo product id
function getProductDetailByProductId(paramProductId) {
  $.ajax({
    url: gBASE_URL + "products/" + paramProductId,
    type: "GET",
    success: function (responseProduct) {
      showDataToUpdateProductModalById(responseProduct);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
// hàm đổ dữ liệu vào Select danh sách các Nhóm sản phẩm trong Create Modal
function pumpDataToProductCategorySelectInCreateModal()
{
  if(gProductCategoriesObjectCreateModal.length === 0) {
    // truy xuất đến phần tử thẻ Select 'Nhóm sản phẩm'
    var vCreateModalProductCategorySelect = $("#create-product-category-select");
    // gán mảng Product Category cho thẻ Select trong Product Update Modal
    gProductCategoriesObjectCreateModal = gProductCategoriesObject;
    // dùng vòng lặp for để duyệt qua từng phần tử trong mảng các đối tượng và đẩy data vào thẻ select
    for(let bI = 0; bI < gProductCategoriesObjectCreateModal.length; bI ++) {
      $("<option>", {
        value: gProductCategoriesObjectCreateModal[bI].categoryCode,
        text: gProductCategoriesObjectCreateModal[bI].categoryName
      }).appendTo(vCreateModalProductCategorySelect);
    }
  }
}
// hàm đổ dữ liệu vào Select danh sách các Quốc gia trong Update Modal
function pumpDataToCountrySelectInCreateModal()
{
  if(gCountriesObjectCreateModal.length === 0) {
    // truy xuất đến phần tử thẻ Select 'Chọn quốc gia'
    var vCreateModalCountrySelect = $("#create-country-select");
    // gán mảng Countries cho thẻ Select trong Product Update Modal
    gCountriesObjectCreateModal = gCountriesObject;
    // dùng vòng lặp for để duyệt qua từng phần tử trong mảng các đối tượng và đẩy data vào thẻ select
    for(let bI = 0; bI < gCountriesObjectCreateModal.length; bI ++) {
      $("<option>", {
        value: gCountriesObjectCreateModal[bI].countryCode,
        text: gCountriesObjectCreateModal[bI].countryName
      }).appendTo(vCreateModalCountrySelect);
    }
  }
}
// hàm đổ dữ liệu vào Select danh sách các Nhóm sản phẩm trong Update Modal
function pumpDataToProductCategorySelectInUpdateModal()
{
  if(gProductCategoriesObjectUpdateModal.length === 0) {
    // truy xuất đến phần tử thẻ Select 'Nhóm sản phẩm'
    var vUpdateModalProductCategorySelect = $("#update-product-category-select");
    // gán mảng Product Category cho thẻ Select trong Product Update Modal
    gProductCategoriesObjectUpdateModal = gProductCategoriesObject;
    // dùng vòng lặp for để duyệt qua từng phần tử trong mảng các đối tượng và đẩy data vào thẻ select
    for(let bI = 0; bI < gProductCategoriesObjectUpdateModal.length; bI ++) {
      $("<option>", {
        value: gProductCategoriesObjectUpdateModal[bI].categoryCode,
        text: gProductCategoriesObjectUpdateModal[bI].categoryName
      }).appendTo(vUpdateModalProductCategorySelect);
    }
  }
}
// hàm đổ dữ liệu vào Select danh sách các Quốc gia trong Update Modal
function pumpDataToCountrySelectInUpdateModal()
{
  if(gCountriesObjectUpdateModal.length === 0) {
    // truy xuất đến phần tử thẻ Select 'Chọn quốc gia'
    var vUpdateModalCountrySelect = $("#update-country-select");
    // gán mảng Countries cho thẻ Select trong Product Update Modal
    gCountriesObjectUpdateModal = gCountriesObject;
    // dùng vòng lặp for để duyệt qua từng phần tử trong mảng các đối tượng và đẩy data vào thẻ select
    for(let bI = 0; bI < gCountriesObjectUpdateModal.length; bI ++) {
      $("<option>", {
        value: gCountriesObjectUpdateModal[bI].countryCode,
        text: gCountriesObjectUpdateModal[bI].countryName
      }).appendTo(vUpdateModalCountrySelect);
    }
  }
}
// hàm load dữ liệu vào Update Product Modal
function showDataToUpdateProductModalById(paramResponseProduct) {
  // gọi hàm load danh sách data vào thẻ Select 'Nhóm sản phẩm'
  pumpDataToProductCategorySelectInUpdateModal();
  // gọi hàm load danh sách data vào thẻ Select 'Chọn quốc gia'
  pumpDataToCountrySelectInUpdateModal();

  // gán nội dung của Product hiện tại cho các thẻ input và select trong Update Product Modal
  $("#input-update-product-code").val(paramResponseProduct.productCode);
  $("#input-update-product-name").val(paramResponseProduct.productName);
  $("#input-update-price").val(paramResponseProduct.price);
  $("#input-update-discount-price").val(paramResponseProduct.discountPrice);
  $("#update-upload-image").attr("src", paramResponseProduct.imagUrl);
  $("#textarea-update-description").val(paramResponseProduct.description);

  // gán giá trị cho value của option hiện tại trong Select 'Nhóm sản phẩm' dựa vào categoryId
  var vCategoryCode = "";
  gProductCategoriesObject.forEach(function(productCategoryItem) {
    if(productCategoryItem.id == paramResponseProduct.categoryId) {
      vCategoryCode = productCategoryItem.categoryCode;
    }
  });
  // gán giá trị cho value của option hiện tại trong Select 'Chọn quốc gia' dựa vào countryId
  var vCountryCode = "";
  gCountriesObject.forEach(function(countryItem) {
    if(countryItem.id == paramResponseProduct.countryId) {
      vCountryCode = countryItem.countryCode;
    }
  });

  // gán nội dung cho thẻ option hiện tại của Select Nhóm sản phẩm và Quốc gia
  $("#update-product-category-select option").filter(function() {
    return $(this).val() == vCategoryCode;
  }).prop('selected', true);
  $("#update-country-select option").filter(function() {
    return $(this).val() == vCountryCode;
  }).prop('selected', true);
}
// hàm lấy thông tin người dùng nhập từ Product Create Modal khi tạo mới sản phẩm
function getCreateNewInputInfor(paramProduct) {
  // lưu thông tin đối tượng Product trên Create Product Modal
  paramProduct.productCode = $("#input-create-product-code").val().trim();
  paramProduct.productName = $("#input-create-product-name").val().trim();
  paramProduct.price = $("#input-create-price").val().trim();
  paramProduct.discountPrice = $("#input-create-discount-price").val().trim();
  paramProduct.description = $("#textarea-create-description").val().trim();
  // gán đường dẫn trực tiếp của ảnh khi đã upload photo thành công lên trang Firebase
  if(gImgUrl == "") {
    paramProduct.imagUrl = $("#create-upload-image").attr("src");
  }
  else {
    paramProduct.imagUrl = gImgUrl;
  }

  // lấy id dựa vào value của thẻ option
  var vProductCategoryOptionValue = $("#create-product-category-select").val();
  var vCountryOptionValue = $("#create-country-select").val();
  gProductCategoriesObjectCreateModal.forEach(function(productCategoryItem) {
    if(productCategoryItem.categoryCode == vProductCategoryOptionValue) {
      // gán CategoryId hiện tại cho thuộc tính CategoryId của Product obj
      paramProduct.categoryId = parseInt(productCategoryItem.id);
    }
  });
  gCountriesObjectCreateModal.forEach(function(countryItem) {
    if(countryItem.countryCode == vCountryOptionValue) {
      // gán CountryId hiện tại cho thuộc tính CountryId của Product obj
      paramProduct.countryId = parseInt(countryItem.id);
    }
  });
}
// hàm lấy thông tin người dùng nhập từ Product Update Modal khi cập nhật lại sản phẩm
function getUpdateCurrentProductInputInfor(paramProduct) {
  // lưu thông tin đối tượng Product trên Update Product Modal
  paramProduct.productCode = $("#input-update-product-code").val().trim();
  paramProduct.productName = $("#input-update-product-name").val().trim();
  paramProduct.price = $("#input-update-price").val().trim();
  paramProduct.discountPrice = $("#input-update-discount-price").val().trim();
  paramProduct.description = $("#textarea-update-description").val().trim();
  // kiểm tra nếu không thực hiện tải lên ảnh mới thì giữ ảnh cũ
  if(gImgUrl == "") { // bằng rỗng nếu không có tải lên ảnh mới
    paramProduct.imagUrl = $("#update-upload-image").attr("src");
  } else { // có tải lên ảnh mới
    paramProduct.imagUrl = gImgUrl;
  }
  // lấy id dựa vào value của thẻ option
  var vProductCategoryOptionValue = $("#update-product-category-select").val();
  var vCountryOptionValue = $("#update-country-select").val();
  gProductCategoriesObjectUpdateModal.forEach(function(productCategoryItem) {
    if(productCategoryItem.categoryCode == vProductCategoryOptionValue) {
      // gán CategoryId hiện tại cho thuộc tính CategoryId của Product obj
      paramProduct.categoryId = parseInt(productCategoryItem.id);
    }
  });
  gCountriesObjectUpdateModal.forEach(function(countryItem) {
    if(countryItem.countryCode == vCountryOptionValue) {
      // gán CountryId hiện tại cho thuộc tính CountryId của Product obj
      paramProduct.countryId = parseInt(countryItem.id);
    }
  });
}
// hàm kiểm tra thông tin các trường nhập vào từ input và select trên Update Product Modal
function validateInputProduct(paramProduct) {
  var vIsValid = true;
  if(paramProduct.productCode.length < 3) {
    alert("Thông tin Product Code phải ít nhất 3 ký tự.");
    vIsValid = false;
  }
  if(paramProduct.productName.length < 10) {
    alert("Thông tin Product Name phải ít nhất 10 ký tự.");
    vIsValid = false;
  }
  if(!isFloatNumber(paramProduct.price)) { // nếu số Number là số thực kiểu float
    alert("Price phải là số thực (kiểu float)");
    vIsValid = false;
  }
  if(parseFloat(paramProduct.price) < 0) {
    alert("Giá trị Price phải lớn hơn 0.");
    vIsValid = false;
  }
  if(!isFloatNumber(paramProduct.discountPrice)) { // nếu số Number là số thực kiểu float
    alert("Discount Price phải là số thực (kiểu float)");
    vIsValid = false;
  }
  if(parseFloat(paramProduct.discountPrice) < 0 || parseFloat(paramProduct.discountPrice) > parseFloat(paramProduct.price)) {
    alert("Giá trị Discount Price phải lớn hơn hoặc bằng 0 \nvà phải nhỏ hơn hoặc bằng giá trị Price");
    vIsValid = false;
  }
  if(paramProduct.categoryId == 0) {
    alert("Phải chọn trong danh sách nhóm sản phẩm.");
    vIsValid = false;
  }
  if(paramProduct.countryId == 0) {
    alert("Phải chọn trong danh sách quốc gia.");
    vIsValid = false;
  }
  if(paramProduct.description.length < 20) {
    alert("Mô tả sản phẩm phải ít nhất 20 ký tự.");
    vIsValid = false;
  }
  return vIsValid;
}
// hàm kiểm tra số thực kiểu Float
function isFloatNumber(paramInput) {  // decimal number
  // Kiểm tra xem giá trị truyền vào có phải kiểu số không
  if (isNaN(paramInput)) {
    return false;
  }
  
  // kiểm tra xem số này có phải là số thực (float) không
  if(!paramInput.includes('.')) {
    return false;
  }

  return true;
}
// hàm gọi api (server) để tạo mới Product theo Product Id
function callAPICreateProductDetail(paramProduct) {
  $.ajax({
    url: gBASE_URL + "/products/",
    type: "POST",
    contentType: gCONTENT_TYPE,
    data: JSON.stringify(paramProduct),
    success: function(res) {
      displayDataWhenCreatedTheProductSuccessful(res);
    },
    error: function(ajaxContext) {
      alert(ajaxContext.status);
    }
  });
} 
// hàm gọi api (server) để cập nhật thông tin mới của Product
function callAPIUpdateProductDetail(paramProduct) {
  $.ajax({
    url: gBASE_URL + "/products/" + gProductId,
    type: "PUT",
    contentType: gCONTENT_TYPE,
    data: JSON.stringify(paramProduct),
    success: function(res) {
      displayDataWhenUpdatedTheProductSuccessful(res);
    },
    error: function(ajaxContext) {
      alert(ajaxContext.status);
    }
  });
}
// hàm call api (server) để xóa sản phẩm theo Product Id
function callAPIDeleteExistingProductById() {
  $.ajax({
    url: gBASE_URL + "/products/" + gProductId,
    type: "DELETE",
    dataType: 'json',
    success: function(res) {
      displayDataWhenDeletedTheProductSuccessful(res);
    },
    error: function(ajaxContext) {
      alert(ajaxContext.status);
    }
  });
}
// hàm xử lý hiển thị khi tạo mới Product thành công
function displayDataWhenCreatedTheProductSuccessful(paramResponse) {
  // đóng Product Create Modal
  $("#create-product-modal").modal("hide");
  // thông báo cập nhật thành công đến người dùng
  alert(`Đã tạo mới sản phẩm: ${paramResponse.productCode} thành công!`);
  // refresh lại trang Products nhưng không tải lại trang
  getAllProducts();
}
// hàm xử lý hiển thị khi cập nhật Product thành công
function displayDataWhenUpdatedTheProductSuccessful(paramResponse) {
  // đóng Product Update Modal
  $("#update-product-modal").modal("hide");
  // thông báo cập nhật thành công đến người dùng
  alert(`Đã cập nhật sản phẩm: ${paramResponse.productCode} thành công!`);
  // refresh lại trang Products nhưng không tải lại trang
  getAllProducts();
}
// hàm xử lý hiển thị khi xóa Product thành công
function displayDataWhenDeletedTheProductSuccessful(paramResponse) {
  // đóng Product Delete Modal
  $("#delete-confirm-modal").modal("hide");
  // thông báo cập nhật thành công đến người dùng
  alert("Đã xóa thành công!");
  // refresh lại trang Products nhưng không tải lại trang
  getAllProducts();
}

// hàm xử lý hiển thị cho nội dung trong Delete Modal
function displayTheContentOfElementsInDeleteModal(paramObj) {
  $("#current-image img").attr("src", paramObj.imagUrl);
  // Tạo một thẻ <span> để bao bọc từng phần tử cần thay đổi
  var vProductNameElement = $("#current-product-name");
  vProductNameElement.html("Sản phẩm:<br> <span class='blue-text h5'>" + paramObj.productName + "</span>");
  // Sử dụng phương thức css() để tô màu xám cho từ "Sản phẩm"
  vProductNameElement.find(".blue-text").css("color", "blue");
  // Sử dụng phương thức css() để tô màu xanh dương cho biến paramObj.productName
  vProductNameElement.find("span:not(.blue-text)").css("color", "blue");
}
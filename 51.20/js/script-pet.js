var gPetID = 1;
// Khai báo mảng chứa các Product để phục vụ cho việc 'Lọc Sản Phẩm'
var gListOfProducts = [];
var gImgUrl = "";
const gBASE_URL = "https://pucci-mart.onrender.com/api";
const gCONTENT_TYPE = "application/json;charset=UTF-8";

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gPET_COLS = [
  "id",
  "type",
  "name",
  "description",
  "imageUrl",
  "price",
  "promotionPrice",
  "discount",
  "createdAt",
  "updatedAt",
  "action",
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gPET_ID_COLS = 0;
const gPET_TYPE_COL = 1;
const gPET_NAME_COL = 2;
const gPET_DESCRIPTION_COL = 3;
const gPET_IMAGE_COL = 4;
const gPET_PRICE_COL = 5;
const gPET_PROMOTIONPRICE_COLS = 6;
const gPET_DISCOUNT = 7;
const gPET_CREATEAT_COL = 8;
const gPET_UPDATEaT_COL = 9;
const gPET_ACTION_COL = 10;
// Cấu hình Firebase SDK với thông tin của dự án Firebase
const gFirebaseConfig = {
  apiKey: "AIzaSyDMcJAAry1dA6vIX2hYmzLrsSKAnXWNbs4",
  authDomain: "photouploadstorage-716e8.firebaseapp.com",
  projectId: "photouploadstorage-716e8",
  storageBucket: "photouploadstorage-716e8.appspot.com",
  messagingSenderId: "297619420631",
  appId: "1:297619420631:web:d1120f69a29ec54bc58792",
  measurementId: "G-CVHX4ED5CQ",
};
// Khởi tạo Firebase
firebase.initializeApp(gFirebaseConfig);
// Khai báo DataTable & mapping collumns
var gContactUserTable = $("#pet-table").DataTable({
  columns: gPET_COLS.map((col, index) => ({
    data: col,
    targets: index,
    render: function (data, type, row) {
      if (col === "imageUrl") {
        return "<img src='" + data + "' alt='Image' width='100' />";
      } else {
        return data;
      }
    },
  })),
  columnDefs: [
    {
      targets: gPET_ACTION_COL,
      className: "action-column",
      defaultContent: `
        <button class='btn btn-primary btn-edit-pet'> Sửa </button>
        <button class='btn btn-danger btn-delete-pet'> Xóa </button>
      `,
    },
  ],
});
$(document).ready(function () {
  // thực hiện tải trang
  onPageLoading();

  // sự kiện nhấn vào button "Thêm mới" ở trang Admin Sản phẩm
  $("#btn-create-contact-user").on("click", function () {
    onBtnAddNewClick();
  });
  // sự kiện ấn vào icon Bút chì để chọn 1 sản phẩm
  $("#pet-table").on("click", ".btn-edit-pet", function () {
    onIconEditClick(this);
  });

  // sự kiện ấn vào icon Recycle để chọn 1 sản phẩm
  $("#pet-table").on("click", ".btn-delete-pet", function () {
    onIconDeleteClick(this);
  });

  // 2. CREATE - sự kiện ấn vào button "Thêm mới" để tạo mới sản phẩm
  $("#btn-create-product").on("click", function () {
    onBtnCreateNewProductModalClick();
  });

  // 3. UPDATE - sự kiện ấn vào button "Cập nhật" để cập nhật thông tin mới của Product
  $("#btn-update-product").on("click", function () {
    onBtnUpdateProductModalClick();
  });

  // 4. DELETE - sự kiện ấn vào button "Xác nhận" để xóa sản phẩm theo Product Id
  $("#btn-confirm-delete-product").on("click", function () {
    onBtnDeleteExistingPetModalClick();
  });

  // sự kiện ấn vào nút button "Lọc sản phẩm"
  //   $("#btn-filter-product").on("click", function () {
  //     onBtnFilterProducts();
  //   });

  // sự kiện nhấn nút 'Tải ảnh lên' ở Create Modal
  $("#file-create-upload").on("change", function (event) {
    onFileUploadPhotoCreatingChange(event);
  });

  // Sự kiện nhấn nút 'Tải ảnh lên' ở Update Modal
  $("#file-update-upload").on("change", function (event) {
    onFileUploadPhotoUpdatingChange(event);
  });

  function onPageLoading() {
    // 1 - R: Read / Load all users
    getAllPet();
  }
  // hàm sự kiện thêm mới sản phẩm trong Product Create Modal
  function onBtnCreateNewProductModalClick() {
    // Khởi tạo đối tượng Product
    var vProduct = initializeProductObject();
    // 1. Đọc dữ liệu
    getCreateNewInputInfor(vProduct);
    // 2. Kiểm tra dữ liệu
    var vValid = validateInputProduct(vProduct);
    if (vValid) {
      // 3. Gọi API
      callAPICreateProductDetail(vProduct);
      // 4. Xử lý hiển thị thông tin sau khi cập nhật Product thành công
      // thực hiện ở hàm displayDataWhenCreatedTheProductSuccessful sau khi cập nhật sản phẩm với thông tin mới thành công
    }
  }
  // hàm sự kiện chọn một sản phẩm và xem chi tiết thông tin
  function onIconEditClick(paramClickedIcon) {
    // lưu thông tin productId đang được edit vào biến toàn cục
    gProductId = getProductIdFromClickedIcon(paramClickedIcon);
    // load thông tin vào các trường dữ liệu trong Update Modal
    getProductDetailByProductId(gProductId);
    // hiển thị Product Update Modal lên
    $("#update-product-modal").modal("show");
  }
  // hàm sự kiện nhấn vào button "Thêm Mới" trang Admin
  function onBtnAddNewClick() {
    // hiển thị Product Update Modal lên
    $("#create-product-modal").modal("show");
  }
  function onBtnUpdateProductModalClick() {
    // Khởi tạo đối tượng Product
    var vProduct = initializeProductObject();
    // 1. Đọc dữ liệu
    getUpdateCurrentProductInputInfor(vProduct);
    // 2. Kiểm tra dữ liệu
    var vValid = validateInputProduct(vProduct);
    if (vValid) {
      // 3. Gọi API
      callAPIUpdateProductDetail(vProduct);
    }
    // 4. Xử lý hiển thị thông tin sau khi cập nhật Product thành công
    // thực hiện ở hàm displayDataWhenUpdatedTheProductSuccessful sau khi cập nhật sản phẩm với thông tin mới thành công
  }
  // hàm sự kiện chọn một sản phẩm và lựa chọn xóa hay không
  function onIconDeleteClick(paramClickedIcon) {
    // lưu thông tin productId đang được delete vào biến toàn cục
    gProductId = getProductIdFromClickedIcon(paramClickedIcon);
    // lấy đối tượng đã tạo chứa thông tin productName và imagUrl để hiển thị lên Modal
    var vTempObj = getSomeNecessaryInforFromClickedIcon(paramClickedIcon);
    displayTheContentOfElementsInDeleteModal(vTempObj);
    // hiển thị Product Delete Modal lên
    $("#delete-confirm-modal").modal("show");
  } // hàm lấy thông tin sản phẩm theo product id
  function getProductDetailByProductId(paramProductId) {
    $.ajax({
      url: gBASE_URL + "/pets/" + paramProductId,
      type: "GET",
      success: function (responseProduct) {
        showDataToUpdateProductModalById(responseProduct);
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      },
    });
  }
  function onBtnDeleteExistingPetModalClick() {
    // 1. Đọc dữ liệu (bỏ qua bước này)
    // 2. Kiểm tra dữ liệu (bỏ qua bước này)
    // 3. Gọi API
    callApiDeleteExistingPetById();
    // 4. Xử lý hiển thị thông tin sau khi cập nhật Product thành công
    // thực hiện ở hàm ... sau khi cập nhật sản phẩm với thông tin mới thành công
  }
  // hàm bao bọc sự kiện change Photo với async/await
  async function onFileUploadPhotoCreatingChange(paramEvent) {
    try {
      var vSelectedFile = paramEvent.target.files[0];

      // kiểm tra phần mở rộng của ảnh tải lên có thỏa điều kiện hay không
      var vFileType = vSelectedFile.type;
      var vFileName = vSelectedFile.name;
      // Kiểm tra phần mở rộng của tên tệp
      var vFileExtension = vFileName.slice(
        ((vFileName.lastIndexOf(".") - 1) >>> 0) + 2
      );
      console.log("vFileExtension: " + vFileExtension);
      // Kiểm tra xem định dạng tệp có phải là ảnh không
      if (
        !vFileType.startsWith("image/") ||
        !["jpg", "jpeg", "png", "gif", "svg"].includes(vFileExtension)
      ) {
        throw new Error(
          "Định dạng tệp không hợp lệ. Chỉ chấp nhận các tệp ảnh (.jpg, .png, .gif, .jpeg, .svg)"
        );
      }

      var vImgUrl = URL.createObjectURL(vSelectedFile);
      $("#create-upload-image").attr("src", vImgUrl);
      // Gọi hàm trả về Promise trước đó và sử dụng await để đợi kết quả
      gImgUrl = await uploadImageToStorageWithPromise(vSelectedFile);
      // Tại đây, bạn có thể sử dụng đường dẫn trực tiếp của hình ảnh (imgUrl) sau khi tải lên thành công
    } catch (error) {
      console.error("Lỗi khi tải ảnh lên:", error);
      // lấy giá trị của src để gán cho biến toàn cục để hiển thị ở form Product Detail
      gImgUrl = $("#create-upload-image").attr("src");
      // Xử lý lỗi ở đây, ví dụ hiển thị thông báo lỗi cho người dùng.
    }
  }
  // hàm lấy thông tin người dùng nhập từ Product Update Modal khi cập nhật lại sản phẩm
  function getUpdateCurrentProductInputInfor(paramProduct) {
    // lưu thông tin đối tượng Product trên Create Product Modal
    paramProduct.type = $("#input-update-pet-type").val().trim();
    paramProduct.name = $("#input-update-pet-name").val().trim();
    paramProduct.price = $("#input-update-price").val();
    paramProduct.promotionPrice = $(
      "#input-update-promotiondiscount-price"
    ).val();

    paramProduct.discount = $("#input-update-discount-price").val();
    paramProduct.description = $("#textarea-update-description").val().trim();
    // gán đường dẫn trực tiếp của ảnh khi đã upload photo thành công lên trang Firebase
    if (gImgUrl == "") {
      paramProduct.imageUrl = $("#update-upload-image").attr("src");
    } else {
      paramProduct.imageUrl = gImgUrl;
    }
  }
  function showDataToUpdateProductModalById(paramResponseProduct) {
    $("#input-update-pet-type").val(paramResponseProduct.type);
    $("#input-update-pet-name").val(paramResponseProduct.name);
    $("#input-update-price").val(paramResponseProduct.price);
    $("#input-update-promotion-price").val(paramResponseProduct.promotionPrice);
    $("#input-update-discount-price").val(paramResponseProduct.discount);
    $("#update-upload-image").attr("src", paramResponseProduct.imageUrl);
    $("#textarea-update-description").val(paramResponseProduct.description);
  }
  function getSomeNecessaryInforFromClickedIcon(paramClickedIcon) {
    var vTemporaryObj = {
      imageUrl: "",
      name: "",
    };
    var vTableRow = $(paramClickedIcon).parents("tr");
    var vProductRowData = gContactUserTable.row(vTableRow).data();
    // gán nội dung cho đối tượng đã tạo
    vTemporaryObj.imageUrl = vProductRowData.imageUrl;
    vTemporaryObj.name = vProductRowData.name;
    return vTemporaryObj;
  }
  //   // hàm bao bọc sự kiện change Photo với async/await
  async function onFileUploadPhotoUpdatingChange(paramEvent) {
    try {
      var vSelectedFile = paramEvent.target.files[0];

      // kiểm tra phần mở rộng của ảnh tải lên có thỏa điều kiện hay không
      var vFileType = vSelectedFile.type;
      var vFileName = vSelectedFile.name;
      // Kiểm tra phần mở rộng của tên tệp
      var vFileExtension = vFileName.slice(
        ((vFileName.lastIndexOf(".") - 1) >>> 0) + 2
      );
      console.log("vFileExtension: " + vFileExtension);
      // Kiểm tra xem định dạng tệp có phải là ảnh không
      if (
        !vFileType.startsWith("image/") ||
        !["jpg", "jpeg", "png", "gif", "svg"].includes(vFileExtension)
      ) {
        throw new Error(
          "Định dạng tệp không hợp lệ. Chỉ chấp nhận các tệp ảnh (.jpg, .png, .gif, .jpeg, .svg)"
        );
      }

      var vImgUrl = URL.createObjectURL(vSelectedFile);
      $("#update-upload-image").attr("src", vImgUrl);
      // Gọi hàm trả về Promise trước đó và sử dụng await để đợi kết quả
      gImgUrl = await uploadImageToStorageWithPromise(vSelectedFile);
      // Tại đây, bạn có thể sử dụng đường dẫn trực tiếp của hình ảnh (imgUrl) sau khi tải lên thành công
    } catch (error) {
      console.error("Lỗi khi tải ảnh lên:", error);
      // gán lại ảnh cũ khi ảnh mới upload lên không hợp lệ
      gImgUrl = $("#update-upload-image").attr("src");
      // Xử lý lỗi ở đây, ví dụ hiển thị thông báo lỗi cho người dùng.
    }
  }
  // hàm tải ảnh lên Firebase và lấy đường dẫn trực tiếp sau khi tải ảnh thành công
  function uploadImageToStorageWithPromise(paramFile) {
    return new Promise((resolve, reject) => {
      // Tạo tham chiếu đến thư mục trong Firebase Storage mà bạn muốn tải lên hình ảnh
      const vStorageRef = firebase
        .storage()
        .ref()
        .child("the-synthetic-product/photo" + paramFile.name);
      // Tải lên hình ảnh lên Firebase Storage
      vStorageRef
        .put(paramFile)
        .then((snapshot) => {
          console.log("Tải lên hình ảnh thành công!");
          // Lấy đường dẫn tới hình ảnh đã tải lên
          return vStorageRef.getDownloadURL();
        })
        .then((url) => {
          console.log("Đường dẫn trực tiếp của hình ảnh:", url);
          // gán đường dẫn URL của image vào biến toàn cục để lưu giá trị
          gImgUrl = url;
          resolve(url); // Trả về đường dẫn trực tiếp của hình ảnh
        })
        .catch((error) => {
          console.error("Lỗi khi tải lên hình ảnh:", error);
          reject(error); // Trả về lỗi nếu có lỗi xảy ra
        });
    });
  }
  function initializeProductObject() {
    // khai báo đối tượng Product
    var vPet = {
      type: "",
      name: "",
      price: 0.0,
      discount: 0.0,
      promotionPrice: 0,
      imageUrl: "",
      description: "",
    };
    return vPet;
  }
  function getAllPet() {
    $.ajax({
      url: gBASE_URL + "/pets/",
      type: "GET",
      success: function (response) {
        // Parse the JSON response
        const paramContactUsers = response.rows;
        // ghi được Response ra console
        gListOfProducts = response;
        loadDataToRegisterUserTable(paramContactUsers);
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      },
    });
  }
  // hàm call api (server) để xóa sản phẩm theo Product Id
  function callApiDeleteExistingPetById() {
    $.ajax({
      url: gBASE_URL + "/pets/" + gPetID,
      type: "DELETE",
      dataType: "json",
      success: function (res) {
        displayDataWhenDeletedPetSuccessful(res);
      },
      error: function (ajaxContext) {
        alert(ajaxContext.status);
      },
    });
  }
  // hàm gọi api (server) để tạo mới Product theo Product Id
  function callAPICreateProductDetail(paramProduct) {
    $.ajax({
      url: gBASE_URL + "/pets",
      type: "POST",
      contentType: gCONTENT_TYPE,
      data: JSON.stringify(paramProduct),
      success: function (res) {
        displayDataWhenCreatedTheProductSuccessful(res);
      },
      error: function (ajaxContext) {
        alert(ajaxContext.status);
      },
    });
  }
  // hàm gọi api (server) để cập nhật thông tin mới của Product
  function callAPIUpdateProductDetail(paramProduct) {
    $.ajax({
      url: gBASE_URL + "/pets/" + gPetID,
      type: "PUT",
      contentType: gCONTENT_TYPE,
      data: JSON.stringify(paramProduct),
      success: function (res) {
        displayDataWhenUpdatedTheProductSuccessful(res);
      },
      error: function (ajaxContext) {
        alert(ajaxContext.status);
      },
    });
  }

  // hàm lấy thông tin người dùng nhập từ Product Create Modal khi tạo mới sản phẩm
  function getCreateNewInputInfor(paramProduct) {
    // lưu thông tin đối tượng Product trên Create Product Modal
    paramProduct.type = $("#input-create-pet-type").val().trim();
    paramProduct.name = $("#input-create-pet-name").val().trim();
    paramProduct.price = $("#input-create-price").val();
    paramProduct.promotionPrice = $("#iinput-create-promotion-price").val();

    paramProduct.discount = $("#input-create-discount-price").val();
    paramProduct.description = $("#textarea-create-description").val().trim();
    // gán đường dẫn trực tiếp của ảnh khi đã upload photo thành công lên trang Firebase
    if (gImgUrl == "") {
      paramProduct.imageUrl = $("#create-upload-image").attr("src");
    } else {
      paramProduct.imageUrl = gImgUrl;
    }
  }
  /** load pet  array to DataTable
   * in: pet array
   * out:pet table has data
   */
  function loadDataToRegisterUserTable(paramContactUsers) {
    gContactUserTable.clear();
    gContactUserTable.rows.add(paramContactUsers);
    gContactUserTable.draw();
  }
  // hàm dựa vào button detail (edit or delete) xác định đc id product
  function getProductIdFromClickedIcon(paramClickedIcon) {
    var vTableRow = $(paramClickedIcon).parents("tr");
    var vProductRowData = gContactUserTable.row(vTableRow).data();
    return vProductRowData.id;
  }
  // hàm kiểm tra thông tin các trường nhập vào từ input và select trên Update Product Modal
  function validateInputProduct(paramProduct) {
    var vIsValid = true;
    if (paramProduct.type == "") {
      alert("Loại không được để trống");
      vIsValid = false;
    }
    if (paramProduct.name == "") {
      alert("tên không được để trống");
      vIsValid = false;
    }
    if (parseFloat(paramProduct.price) < 0) {
      alert("Giá trị Price phải lớn hơn 0.");
      vIsValid = false;
    }
    if (
      parseFloat(paramProduct.promotionPrice) < 0 ||
      parseFloat(paramProduct.promotionPrice) <= parseFloat(paramProduct.price)
    ) {
      alert(
        "Giá trị Discount Price phải lớn hơn hoặc bằng 0 \nvà phải nhỏ hơn hoặc bằng giá trị Price"
      );
      vIsValid = false;
    }
    if (paramProduct.description.length < 5) {
      alert("Mô tả sản phẩm phải ít nhất 5 ký tự.");
      vIsValid = false;
    }
    return vIsValid;
  }
  // hàm xử lý hiển thị khi tạo mới Product thành công
  function displayDataWhenCreatedTheProductSuccessful(paramResponse) {
    // đóng Product Create Modal
    $("#create-product-modal").modal("hide");
    // thông báo cập nhật thành công đến người dùng
    alert(`Đã tạo mới sản phẩm: ${paramResponse.name} thành công!`);
    // refresh lại trang Products nhưng không tải lại trang
    getAllPet();
  }
  // hàm xử lý hiển thị khi cập nhật Product thành công
  function displayDataWhenUpdatedTheProductSuccessful(paramResponse) {
    // đóng Product Update Modal
    $("#update-product-modal").modal("hide");
    // thông báo cập nhật thành công đến người dùng
    alert(`Đã cập nhật sản phẩm: ${paramResponse.name} thành công!`);
    // refresh lại trang Products nhưng không tải lại trang
    getAllPet();
  }
  // hàm xử lý hiển thị khi xóa Product thành công
  function displayDataWhenDeletedPetSuccessful(paramResponse) {
    // đóng Product Delete Modal
    $("#delete-confirm-modal").modal("hide");
    // thông báo cập nhật thành công đến người dùng
    alert("Đã xóa thành công!");
    // refresh lại trang Products nhưng không tải lại trang
    getAllPet();
  }
  function displayTheContentOfElementsInDeleteModal(paramObj) {
    $("#current-image img").attr("src", paramObj.imageUrl);
    // Tạo một thẻ <span> để bao bọc từng phần tử cần thay đổi
    var vProductNameElement = $("#current-product-name");
    vProductNameElement.html(
      "Sản phẩm:<br> <span class='blue-text h5'>" + paramObj.name + "</span>"
    );
    // Sử dụng phương thức css() để tô màu xám cho từ "Sản phẩm"
    vProductNameElement.find(".blue-text").css("color", "blue");
    // Sử dụng phương thức css() để tô màu xanh dương cho biến paramObj.productName
    vProductNameElement.find("span:not(.blue-text)").css("color", "blue");
  }
});
